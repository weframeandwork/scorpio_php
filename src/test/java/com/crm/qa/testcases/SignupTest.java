package com.crm.qa.testcases;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeMethod;

import com.crm.qa.base.Base;
import com.crm.qa.pages.SignupPage;

public class SignupTest extends Base {
	
	final static Logger logger = Logger.getLogger(SignupTest.class);

	SignupPage signup;
	public SignupTest() {
		super();
	}

	@BeforeMethod
	
	public void setup()
	{
		initialization();
		signup= new SignupPage(driver);
	}
}
