package com.crm.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.crm.qa.base.Base;

public class SignupPage extends Base {

	WebDriver driver;
	By username = By.id("user_login");
	By password = By.xpath(".//*[@id='user_pass']");
	By signupButton = By.name("wp-submit");

	public SignupPage(WebDriver driver) {
		this.driver = driver;
	}

	public void register(String userid, String pass) {

		driver.findElement(username).sendKeys(userid);
		driver.findElement(password).sendKeys(pass);
		driver.findElement(signupButton).click();

	}
	
	
}
