# PHPTravelApplication
### Important Notes:
1. Member who joins in a team are supposed to make a new branch.
   The branch name syntax has to be in the format : PHP_(Person Name).
   For Instance : PHP_Piyush
2. Default Reviewer for this branch is : Samreen Adil
3. Every member should commit code only their sepecific branch with a proper commit message that describes his/her changes in the code.
4. After a member leaves a team or after the project ends, he/she should delete his branch provided that his pull requests have been approved by the owner and the code has been successfully merged to the master.
5. Team Members cannnot commit the code to the master branch, only default reviewers have those rights.
6. Members have no rights to create their own repository inside this organization.
7. It is not recommended to fork this repository into your personal github or bitbucket account.
8. You can contact to owner, if any queries write at: pcm708automation@gmail.com